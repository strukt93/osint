## Social media
This file contains links/tools/resources that help in social media-related information gathering.

- https://github.com/sherlock-project/sherlock: Queries lots of websites to check if the supplied username exists on them
- https://github.com/Greenwolf/social_mapper: Queries many social media websites to find potential targets
- https://github.com/webbreacher/whatsmyname: Queries lots of websites to check if the supplied username exists on them