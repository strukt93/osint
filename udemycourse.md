### OSINT Frameworks
#### This section contains links to OSINT frameworks as well as web pages that link to multiple and general tools:
- https://osintframework.com: A framework that categorizes sources by type, with links to all of the useful resources under each category
- https://one-plus.github.io/access.html: Conduct searches in the different social media as well as other sources
- https://start.me/p/rx6Qj8/start-page: A repository that contains a great amount of tools, each in their respective sections
- http://www.aware-online.com/osint-tools (Not provided by the course, alternative to inteltechniques)
- https://www.osinttechniques.com/osint-tools.html (Not provided by the course, alternative to inteltechniques)

### Reverse email search
#### This section contains links to reverse email search services:
- https://www.beenverified.com (paid)
- https://infotracer.com (paid)
- Google dork: "email@example.com"
- Facebook forgot password page
- https://haveibeenpwned.com (free)
- Maltego

### Reverse image search
#### This section contains links to reverse image search services:
- Google images
- https://www.tineye.com
- Bing images

### Website OSINT
#### This section contains info about website OSINT:
- Spiderfoot
- HTTrack
- Metagoofil
- Wayback machine (https://wayback.archive.org)
- https://inteltechniques.com (tools section, though it was removed recently)
- https://osintframework.com
- https://www.uk-osint.net

### People search
#### This section contains info about people search services:
- https://www.blackbookonline.info (appears to be US only)
- https://www.xlek.com (appears to be US only)

### The darkweb
#### This section contains info about the darkweb:
- https://thehiddenwiki.org
- Tor

### ID security
#### This section contains info about identity security:
- Sudo

### Browser plugins/extensions
#### This section contains info about browser extensions:
- SurfSafe
- VideoDownloadHelper
- FireShot
- Nimbus Capture
- User-Agent Switcher

### Data breaches
#### This section contains info about data breach databases:
- https://haveibeenpwned.com
- https://www.dehashed.com

### Social media
#### This section contains info about social media:
- https://tinfoleak.com (https://github.com/vaguileradiaz/tinfoleak)
- https://inteltechniques.com (tools section, though it was removed recently)
- https://webmii.com
- https://pitoolbox.com.au/facebook-tool/ (not working since Facebook brought graph search down)